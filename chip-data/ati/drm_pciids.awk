#!/usr/bin/gawk -f

BEGIN {
	FS=", "

	# NOTE: please edit flags template as well
	flag_letter["RADEON_IS_MOBILITY"] = "m"
	flag_letter["RADEON_NEW_MEMMAP"] = "M"
	flag_letter["RADEON_IS_IGP"] = "i"
	flag_letter["RADEON_IS_IGPGART"] = "I"
	flag_letter["RADEON_SINGLE_CRTC"] = "c"
	flags_template = "mMiIc"
	first_line = ""
	printf("{\n")
}

function error(f, s1, s2) {
	printf("%s:%d: " f "\n", FILENAME, NR, s1, s2) >"/dev/stderr"
	error_code = 1
}

function flag_to_letter(f_name)
{
	if (f_name in flag_letter) {
		return flag_letter[f_name]
	} else {
		error("unknown flag %s%s", f_name)
		return f_name
	}
}

function format_flags(f_code,        i, n, l, out) {
	n = length(flags_template)
	for (i=1; i<=n; i++) {
		l = substr(flags_template, i, 1)
		out = out f_code[l]
		delete f_code[l]
	}
	for (l in f_code) {
		out = out f_code[l]
		if (length(l)==1)
			error("flag \"%s\" missing from flags_template", l, "")
	}
	if (length(out)>0) {
	    out = ",\"" out "\""
	}
	return out
}
		
/^[[:blank:]]+{0x/ {
	sub(/^[[:blank:]]+{/,"")
	sub(/},[[:blank:]]*\\$/,"")

	id_vendor = $1; sub(/^0x/, "", id_vendor)
	id_type = $2; sub(/^0x/, "", id_type)

	flags = $7

	if (flags == "0")
		next

	n_flag_arr = split(flags, flag_arr, "|")
	first_flag = flag_arr[1]
	delete f_code
	for (i=2; i<=n_flag_arr; i++) {
		l = flag_to_letter(flag_arr[i])
		f_code[l] = l
	}

	formatted_flags = format_flags(f_code)

	# second param to gensub is the replacement text:
	## chip_type = tolower(gensub(/^CHIP_/, "", "", first_flag))
	chip_type = gensub(/^CHIP_/, "", "", first_flag)

    if (length(first_line) > 0) {
        printf(",\n")
    } else {
        first_line = "TRUE"
    }
	if ((chip_type in types) && types[chip_type] != first_flag)
		error("chip name collision: %s vs. %s", \
			types[chip_type], first_flag)

	types[chip_type] = first_flag

	printf("\t\"%s,%s\":[\"%s\"%s]", toupper(id_vendor), toupper(id_type),\
	    chip_type, formatted_flags)
}

END {
    printf("\n}\n")
	exit error_code
}
