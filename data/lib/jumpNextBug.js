// Released under the MIT/X11 license
// http://www.opensource.org/licenses/mit-license.php
/*global isInList */

(function createRelationElements() {
  "use strict";
  var relation = {}, key = null;
  var linkLabels = [
      "First", "Last", "Prev", "Next"
    ];
  var labelToRelation = {
    "First" : {
      rel : "start"
    },
    "Last" : {
      rel : "last"
    },
    "Prev" : {
      rel : "prev"
    },
    "Next" : {
      rel : "next"
    }
  };
  var nextRE = new RegExp("Next", "i");

  function createLinkRel(rel, href) {
    var newLinkElement = document.createElement("link");
    newLinkElement.setAttribute("rel", rel);
    newLinkElement.setAttribute("href", href);
    document.getElementsByTagName("head")[0]
      .appendChild(newLinkElement);
  }

  var aNavigElements = document
      .querySelectorAll("#bugzilla-body .navigation a");
  Array.forEach(aNavigElements, function (elem) {
    var labelText = elem.textContent.trim();
    if (isInList(labelText, linkLabels)) {
      labelToRelation[labelText].href = elem
        .getAttribute("href");

      if (nextRE.test(labelText)) {
        elem.setAttribute("accesskey", "n");
        elem.innerHTML = "<u>N</u>ext";
      }
    }
  });

  for (key in labelToRelation) {
    if (labelToRelation.hasOwnProperty(key)) {
      relation = labelToRelation[key];
      if (relation.href) {
        createLinkRel(relation.rel, relation.href);
      }
    }
  }
}());
