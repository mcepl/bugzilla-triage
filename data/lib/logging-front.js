// Released under the MIT/X11 license
// http://www.opensource.org/licenses/mit-license.php
"use strict";

var EmptyLogsColor = new Color(0, 255, 0);
var FullLogsColor = new Color(0, 40, 103);

var submitHandlerInstalled = false; // for setUpLogging

function addLogRecord() {
  var rec = {};
  rec.date = new Date();
  rec.url = document.location.toString();
  rec.title = document.title;
  var comment = window.prompt("Enter comments for this comment");
  if (comment !== null) {
    comment = comment.trim();
    if (comment.length > 0) {
      comment = comment.trim();
      rec.comment = comment;
      var dateStr = getISODate(rec.date);
      var urlStr = window.location.hostname;
      var bugNo = getBugNoFromURL(window.location.href);
      rec.key = dateStr + "+" + urlStr + "+" + bugNo;
      self.postMessage(new Message("AddLogRecord", rec));
    }
    return rec;
  }
  return null;
}

/**
 */
function setUpLogging() {
  // Protection against double-call
  if (document.getElementById("generateTSButton")) {
    return;
  }

  // For adding additional buttons to the top toolbar
  var additionalButtons = document
      .querySelector("#bugzilla-body *.related_actions");
  var that = this;

  // logging all submits for timesheet
  if (!submitHandlerInstalled) {
    document.forms.namedItem("changeform").addEventListener(
        "submit", function(evt) {
          if (addLogRecord() === null) {
            evt.stopPropagation();
            evt.preventDefault();
          }
        }, false);
    submitHandlerInstalled = true;
  }

  // (id, text, parent, callback, params, before, covered, accesskey)
  createDeadLink("generateTSButton", "Generate TS",
      additionalButtons, function(evt) {
        self.postMessage(new Message("GetTSData"));
      }, [], "dash", "li");

  createDeadLink("clearLogs", "Clear TS", additionalButtons,
      function(evt) {
        self.postMessage(new Message("ClearTS"));
      }, [], "dash", "li");

  createDeadLink("importTSButton", "Import TS",
      additionalButtons, function(evt) {
        self.postMessage(new Message("ImportTS"));
      }, [], "dash", "li");

  /*
   * TODO var clearLogAElem = document.getElementById("clearLogs"); if
   * (this.log.isEmpty()) { clearLogAElem.style.color = this.log.EmptyLogsColor;
   * clearLogAElem.style.fontWeight = "normal"; } else {
   * clearLogAElem.style.color = this.log.FullLogsColor;
   * clearLogAElem.style.fontWeight = "bolder"; }
   */
}


function getBugzillaAbbr(url) {
  // Abbreviations are in constantData.BugzillaAbbreviations
  // for https://bugzilla.redhat.com/show_bug.cgi?id=579123 get RH
  // for https://bugzilla.mozilla.org/show_bug.cgi?id=579123 get MoFo
  return constantData.BugzillaAbbreviations[parseURL(url).host];
}

function timeSheetRecordsPrinter(records, date) {
  var commentBugRE = new RegExp("[bB]ug\\s+([0-9]+)", "g");
  // sort the records into temporary array
  var tmpArr = [];
  var dateStr = getISODate(date);
  var outStr = "data:text/html;charset=utf-8," +
      '<!DOCTYPE html>' + "<html><head>\n" +
      "<meta charset='utf-8'/>\n" + "<title>TimeSheet-" +
      dateStr + "</title>\n</head>\n<body>\n" +
      "<h1>TimeSheet</h1>\n";

  for ( var i in records) {
    if (records.hasOwnProperty(i)) {
      tmpArr.push([
          i, records[i]
      ]);
    }
  }
  tmpArr.sort(function(a, b) {
    if (a[0] > b[0]) {
      return 1;
    }
    else if (a[0] < b[0]) {
      return -1;
    }
    else {
      return 0;
    }
  });

  var currentDay = "";
  // now print the array
  tmpArr.forEach(function(rec) {
    var x = rec[1];
    var dayStr = getISODate(x.date);
    var host = parseURL(x.url).host;
    var BZName = getBugzillaAbbr(x.url);
    var bugNo = getBugNo(x.url);
    if (dayStr != currentDay) {
      currentDay = dayStr;
      outStr += "<hr/><p><strong>" + currentDay
          + "</strong></p>\n";
    }
    // replace "bug ####" with a hyperlink to the current bugzilla
    var comment = x.comment.replace(commentBugRE,
        "<a href='http://" + host
            + "/show_bug.cgi?id=$1'>$&</a>");
    outStr += "<p><em><a href='" + x.url + "'>Bug " + BZName
        + "/" + bugNo + ": " + x.title + "</a>"
        + " </em>\n<br/>" + comment + "</p>\n";
  });
  outStr += "</body></html>";
  self.postMessage(new Message("OpenURLinTab", outStr));
};
