//Released under the MIT/X11 license
//http://www.opensource.org/licenses/mit-license.php
"use strict";

// Make a JSONL-RPC call ... most of the business logic should stay in the
// content script
// http://json-rpc.org/wd/JSON-RPC-1-1-WD-20060807.html
function makeJSONRPCCall(method, params, callback) {

  var msg = {
      "version": "1.1",
      "method": method,
      "params": params
  };

  myDebug("makeJSONRPCCall(" + method +
    "): out = " + JSON.stringify(msg));

  var req = new XMLHttpRequest();
  req.open('POST', "/jsonrpc.cgi", true);
  req.onreadystatechange = function (aEvt) {
    if (req.readyState == 4) {
      if(req.status == 200) {
        myDebug("makeJSONRPCCall (" + method +
          "): in = " + req.responseText);
        var JSONresponse = JSON.parse(req.responseText);
        if ("error" in JSONresponse) {
          throw new Error("Error in JSON-RPC call:\n" +
            JSONresponse.error.toSource());
        }
        callback(JSONresponse);
      }
      else {
        console.error('Error', req.statusText);
      }
    }
  };
  req.setRequestHeader('Content-Type', "application/json");
  req.send(JSON.stringify(msg));
};
