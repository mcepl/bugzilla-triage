/*jslint es5: true, vars: true, plusplus: true, maxerr: 50, indent: 2,
  devel: true, browser: true, vars: true, forin: true */
/*global parseMailto, ISODateString, parseBZCommentDate, getReporter,
  ReporterColor, constantData, createDeadLink, fixAttachById, addTextLink,
  parseURL, isInList, analyzeXorgLog, config */
// Released under the MIT/X11 license
// http://www.opensource.org/licenses/mit-license.php
"use strict";

function Comment(comment) {
  var nameSpan = comment.querySelector(".bz_comment_user a.email");
  var timeSpan = comment.getElementsByClassName("bz_comment_time")[0];

  this.author = parseMailto(nameSpan).trim();
  this.element = comment;
  this.date = parseBZCommentDate(timeSpan.textContent.trim());
  this.timeSpan = timeSpan;
}

Comment.prototype.getText = function getText() {
  return (this.element.getElementsByTagName("pre")[0].textContent.trim());
};

function CommentList(doc) {
  var commentElems = document.getElementById("comments").
    getElementsByClassName("bz_comment");
  var comments = {};
  Array.forEach(commentElems, function (item) {
    var com = new Comment(item);
    if (com.element) {
      comments[ISODateString(com.date)] = com;
    }
  });
  this.comments = comments;
}

/**
 * Set background color of all comments made by reporter in ReporterColor color
 *
 */
CommentList.prototype.colorComments = function colorComments() {
  var reporter = getReporter();
  var com = null, idx = null;
  for (idx in this.comments) {
    com = this.comments[idx];
    if (com.author === reporter) {
      com.element.style.backgroundColor = ReporterColor.toString();
    }
  }
};

CommentList.prototype.getAllCommentsText =  function getAllCommentsText() {
  var outStr = "", idx = null;
  for (idx in this.comments) {
    outStr += this.comments[idx].getText() + "\n";
  }
  return outStr;
};
// -----------------------------------------------------------

/**
 * Parse the row with the attachment and create new Attachment object
 *
 * @param DOM
 *          element to be parsed
 *
 * TODO error handling is missing ... it's bleee
 *
 * [ attName, id, MIMEtype, size, inElem ];
 */
function Attachment(inElem) {
  // Skip over obsolete attachments
  if (inElem.getElementsByClassName("bz_obsolete").length > 0) {
    return; // FIXME how NOT to create an object?
  }

  // getting name of the attachment
  this.name = inElem.getElementsByTagName("b")[0].textContent.trim();

  // TODO probably could use url.URL object
  var aHrefsArr = inElem.getElementsByTagName("a");
  var aHref = Array.filter(aHrefsArr, function (x) {
    return x.textContent.trim() === "Details";
  })[0];
  this.id = parseURL(aHref.getAttribute("href")).params.id;

  // getting MIME type and size
  var stringArray = inElem.getElementsByClassName("bz_attach_extra_info")[0].
    textContent.replace(/[\n ()]+/g, " ").trim().split(", ");
  this.bugId = getBugNo();
  this.size = parseInt(stringArray[0], 10);
  this.mimeType = stringArray[1].split(" ")[0];
  this.element = inElem;
}

Attachment.prototype.isBadMIME = function isBadMIME() {
  var badMIMEArray = [ "application/octet-stream", "text/x-log", "undefined" ];
  return isInList(this.mimeType, badMIMEArray);
};

Attachment.prototype.checkAttLink = function checkAttLink(baseIDname, reIdx) {
  var elemS = this.element.getElementsByTagName("td");
  var elem = elemS[elemS.length - 1];
  createDeadLink(baseIDname + "_" + this.id, "check", elem,
      analyzeXorgLog, [this.id, reIdx], "br");
};

Attachment.prototype.isParsed = function isParsed() {
  var titleParsedAttachment = "Part of the thread where crash happened";
  return (new RegExp(titleParsedAttachment).test(this.name));
};

// ----------------------------------------------------------------------------
function AttachList(doc) {
  this.attachments = [];
  var attach = {}, i = 0, ii = 0;
  var attElements = doc.getElementById("attachment_table").
    getElementsByTagName("tr");
  // FIXME change into list of objects and both comments and
  // attachments (and something else?) should be properties of one
  // huge object
  for (i = 1, ii = attElements.length - 1; i < ii; i++) {
    attach = new Attachment(attElements[i]);
    if (attach.id) {
      this.attachments.push(attach);
    }
  }
}

AttachList.prototype.getBadAttachments = function getBadAttachments() {
  return this.attachments.filter(function (att) {
    return (att.isBadMIME());
  });
};

/**
 * Add a link opening selected lines of Xorg.0.log
 */
AttachList.prototype.addCheckXorgLogLink = function addCheckXorgLogLink() {
  if (config.XorgLogAnalysis) {
    this.getAttList(/[xX].*log/).
      forEach(function (att) {
        att.checkAttLink("xorgLogAnalyzeLink", "AnalyzeXorgLogBacktrace");
      });
    this.getAttList(/[dD]mesg/).
      forEach(function (att) {
        att.checkAttLink("dmesgAnalyzeLink", "AnalyzeDmesgErrors");
      });
  }
};

/**
 * Make it sailent that the some attachments with bad MIME type are present
 *
 * @param atts
 *          Array of attachments subarrays
 * @return none
 */
AttachList.prototype.markBadAttachments = function markBadAttachments() {
  if (!constantData.passwordState.passAvailable) {
    // No password for XML-RPC service, no XML-RPC service, so simple!
    return null;
  }

  var badAttachments = this.getBadAttachments();

  if (badAttachments.length > 0) {
    var titleElement = document.
      getElementsByClassName("bz_alias_short_desc_container")[0];
    titleElement.style.backgroundColor = "olive";

    createDeadLink("fixAllButton", "Fix all", titleElement, function () {
      Array.forEach(badAttachments, function (x) {
        fixAttachById(x.bugId, x.id);
      });
    }, [], false, null, "f");
    badAttachments.forEach(function (x, i, a) {
      addTextLink(x);
    });
  }

  return true;
};

AttachList.prototype.getParsedAttachments = function getParsedAttachments() {
  return this.attachments.filter(function (att) {
    return (att.isParsed());
  });
};

AttachList.prototype.getAttList = function getAttList(attRE) {
  return this.attachments.filter(function (value) {
    // Xorg.0.log must be text, otherwise we cannot parse it
    return (attRE.test(value.name) && /text/.test(value.mimeType));
  });
};

AttachList.prototype.forEach = function forEach(fce) {
  this.attachments.forEach(fce);
};
