// Released under the MIT/X11 license
// http://www.opensource.org/licenses/mit-license.php
"use strict";

function getSel() {
  var text = "";
  var selectionObject = window.getSelection();
  if (selectionObject.rangeCount > 0) {
    text = selectionObject.getRangeAt(0).toString().trim();
  }
  return text;
}

/**
 * Opens a new tab with a query for the given text in the selected component
 *
 * @param text
 *          to be searched for
 * @param component
 *          String with the component name (maybe latter regexp?)
 * @param product
 *          (optional) string with the product name, if undefined, search in all
 *          products
 * @return None
 *
 */
function queryInNewTab(text, component, product, equivComps) {
  var urlStr = "https://" + window.location.hostname
      + "/buglist.cgi?query_format=advanced";
  if (product) {
    urlStr += "&product=" + product.trim();
  }
  if (component) {
    if (equivComps) {
      var equivCompsArr = equivComps.filter(function(REstr) {
        return new RegExp(REstr).test(component);
      }, this);
      if (equivCompsArr.length > 0) {
        component = equivCompsArr[0];
      }
    }
    urlStr += "&component=" + component.trim();
  }

  // using more complicated query tables here, because they can be more easily
  // edited
  // for further investigative searches
  if (text) {
    text = encodeURIComponent(text.trim());
    var searchText = "&field0-0-0=longdesc&type0-0-0=substring&value0-0-0="
        + text
        + "&field0-0-1=attach_data.thedata&type0-0-1=substring&value0-0-1="
        + text
        + "&field0-0-2=status_whiteboard&type0-0-2=substring&value0-0-2="
        + text;
    urlStr += searchText;
    self.postMessage(new Message("OpenURLinTab", urlStr)); // utils.js is
    // always avaiulable
  }
}

/**
 * Get the text to search for and prepare other things for the real executive
 * function this.queryInNewTab, and run it.
 */
function queryForSelection() {
  var text = getSel();
  if (!text) {
    self.postMessage(new Message("GetClipboard", "queryLocal"));
  }
  else {
    if (equivalentComponents) {
      queryInNewTab(text, getComponent(), getProduct(),
          equivalentComponents);
    }
    else {
      queryInNewTab(text, getComponent(), getProduct());
    }
  }
}

/**
 *
 */
function queryUpstreamCallback(text, queryUpBug) {
  var searchData = filterByRegexp(queryUpBug, getComponent());
  if (!searchData) {
    return; // not sure why it should happen, but certainly better
  }
  var urlBase = searchData.url;
  text = searchData.searchBy + ":" + searchData.fillIn + " "
      + text.trim();
  if (searchData.fillIn == "$$$") {
    text = text.replace("$$$", getComponent());
  }
  text = encodeURIComponent(text).replace("%20", "+");
  self.postMessage(new Message("OpenURLinTab", urlBase + text));
}

/**
 * Search simple query in the upstream bugzilla appropriate for the component
 *
 * @return none
 */
function queryUpstream(qUpBug) {
  if (!qUpBug) {
    alert("We don't have constantData.queryUpstreamBug set up!");
    return null;
  }
  var text = getSel();
  if (!text) {
    self
        .postMessage(new Message("GetClipboard", "queryUpstream"));
  }
  else {
    queryUpstreamCallback(text, qUpBug);
  }
  return true;
}
