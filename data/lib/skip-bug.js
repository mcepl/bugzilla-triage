// Released under the MIT/X11 license
// http://www.opensource.org/licenses/mit-license.php
// "use strict";

function reloadPage() {
	var titleElems = document.getElementsByTagName('title');
	if (titleElems) {
		var REArr = new RegExp('[bB]ug\\s+([0-9]+)')
				.exec(titleElems[0].textContent);
		if (REArr) {
			var URLArr = document.location.pathname.split("/");
			document.location = URLArr.slice(0, URLArr.length - 1).join("/")
					+ "/show_bug.cgi?id=" + REArr[1];
		}
	}
}

reloadPage();
