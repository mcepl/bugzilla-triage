// Released under the MIT/X11 license
// http://www.opensource.org/licenses/mit-license.php
"use strict";

/**
 * Find default assignee based on the current component
 *
 * @return String what would be a default assignee if we haven't set it up.
 */
function getDefaultAssignee() {
  return filterByRegexp(constantData.defaultAssignee,
      getComponent());
}

/**
 * Set default assignee
 *
 * @return none sets this.defaultAssignee property according to defaultAssignee
 *         list
 */
function setDefaultAssignee() {
  var defAss = getDefaultAssignee();

  // Add setting default assignee
  if (defAss && (defAss !== getOwner())) {
    createNewButton("bz_assignee_edit_container", true, {
      "name" : "Def. Assignee",
      "assignee" : "default"
    });
  }
}

function markBugTriaged() {
  // https://fedoraproject.org/wiki/BugZappers/Meetings/Minutes-2009-Oct-27
  // http://meetbot.fedoraproject.org/fedora-meeting/2009-11-24\
  // /fedora-meeting.2009-11-24-15.11.log.html
  // http://meetbot.fedoraproject.org/fedora-meeting/2009-11-24\
  // /fedora-meeting.2009-11-24-15.11.log.html
  if (hasXorgBugsCategory && !hasXorgBugsCategory()) {
    alert("This won't do! First set the category!");
  }
  else if (isEnterprise && !isEnterprise()
      && (getSeverity() == 'unspecified')) {
    alert("This won't do! Specify some severity!");
  }
  else {
    addStuffToTextBox("keywords", "Triaged");
  }
}

var FillMagicDoneRE = new RegExp("^\\s*\\[[0-9a-zA-Z_]*\\]");

function addingEmbelishments(logList) {

  var maintCCAddr = "";
  if (constantData.CCmaintainer) {
    maintCCAddr = filterByRegexp(constantData.CCmaintainer,
        getComponent()); // filterByRegexp returns string, not array, now
  }

  // we should make visible whether maintCCAddr is in CCList
  if (maintCCAddr && isInList(maintCCAddr, getCCList())) {
    var ccEditBoxElem = document
        .getElementById("cc_edit_area_showhide");
    ccEditBoxElem.style.color = "navy";
    ccEditBoxElem.style.fontWeight = "bolder";
    ccEditBoxElem.style.textDecoration = "underline";
  }

  // Take care of signature for Fedora bugzappers
  if (config.signature && config.signature.length > 0) {
    var signaturesCounter = 0;
    var signatureFedoraString = config.signature;
    document.forms.namedItem("changeform").addEventListener(
        "submit", function(aEvt) {
          if (signaturesCounter < 1) {
            addStuffToTextBox("comment", signatureFedoraString);
            signaturesCounter += 1;
          }
        }, false);
  }

  // set default assignee on change of the component
  var compElement = document.getElementById("component");
  if (compElement && (compElement.options)) {
    document.getElementById("component").addEventListener(
        "change", function() {
          changeAssignee("default");
        }, false);
  }

  // TODO Get compiz bugs as well
  if ((new Boolean(constantData.chipNames))
      && (logList.length > 0) && (!isTriaged())
      && (!FillMagicDoneRE.test(getSummary()))
      && (maintCCAddr == "xgl-maint@redhat.com")) {
    // Add find chip magic button
    analyzeXorgLog(logList[0].id, "AnalyzeInterestingLine");
  }

  // Add links for creating new bug in the same product
  // and same component
  addNewLinks();
}

/**
 * Set branding colours to easily distinguish between Fedora and RHEL bugs
 *
 * @param brand
 *          String with product of the current bug
 * @param version
 *          String with the version of the bug
 * @param its
 *          String with the IsueTracker numbers
 * @return none
 */
function setBranding(things) {
  var brandColor = {};
  var TriagedColor = {};

  var atts = things.attachments;
  var xLogAtts = atts.getAttList(/[xX].*log/);

  var ITbutton = document.getElementById("cf_issuetracker");
  var its = ITbutton ? ITbutton.value.trim() : "";

  if (isEnterprise()) {
    if (its && (its.length > 0)) {
      brandColor = RHITColor;
    }
    else {
      brandColor = RHColor;
    }
  }
  else if (new RegExp("Fedora").test(document
      .getElementById("product").value)) {
    if (document.getElementById("version").value === "rawhide") {
      brandColor = RawhideColor;
    }
    else {
      brandColor = FedoraColor;
    }
  }

  // Comment each of the following lines to get only partial branding
  document.getElementsByTagName("body")[0].style.background = brandColor
      .toString()
      + " none";
  document.getElementById("titles").style.background = brandColor
      .toString()
      + " none";

  var bodyTitleParent = document
      .getElementById("summary_alias_container").parentNode;
  var bodyTitleElem = bodyTitleParent.getElementsByTagName("b")[0];
  bodyTitleElem.textContent = bodyTitleElem.textContent.slice(4);

  // Make background-color of the body of bug salmon pink
  // for security bugs.
  if (hasKeyword("Security")) {
    document.getElementById("bugzilla-body").style.background = SalmonPink
        .toString()
        + ' none';
  }

  // Make it visible whether the bug has been triaged
  if (isTriaged()) {
    document.getElementById("bz_field_status").style.background = brandColor
        .lightColor().toString()
        + " none";
  }

  var compElems;
  if (config.suspiciousComponents
      && isInList(getComponent(), config.suspiciousComponents)
      && ((compElems = document
          .getElementById("bz_component_edit_container")))) {
    compElems.style.background = "red none";
  }

  addingEmbelishments(xLogAtts);
}
