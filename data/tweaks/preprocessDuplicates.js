/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Bugzilla Tweaks.
 *
 * The Initial Developer of the Original Code is Mozilla Foundation.
 * Portions created by the Initial Developer are Copyright (C) 2010
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Johnathan Nightingale <johnath@mozilla.com>
 *   Ehsan Akhgari <ehsan@mozilla.com>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

function preprocessDuplicateMarkers(mainDoc, histDoc) {
  var comments = mainDoc.querySelectorAll(".bz_comment");
  var reDuplicate = /^\s*\*\*\*\s+Bug\s+(\d+)\s+has\s+been\s+marked\s+as\s+a\s+duplicate\s+of\s+this\s+bug.\s+\*\*\*\s*$/i;
  var row = 0;
  var rows = histDoc.querySelectorAll("#bugzilla-body tr");
  for ( var i = 1 /* comment 0 can never be a duplicate marker */; i < comments.length; ++i) {
    var textHolder = comments[i]
        .querySelector(".bz_comment_text");
    var match = reDuplicate.exec(trimContent(textHolder));
    if (match) {
      // construct the table row to be injected in histDoc
      var bugID = match[1];
      var email = comments[i]
          .querySelector(".bz_comment_user .email").href
          .substr(7);
      var link = textHolder.querySelector("a");
      var title = link.title;
      var time = trimContent(comments[i]
          .querySelector(".bz_comment_time"));
      var what = 'Duplicate';
      var removed = '';
      var number = trimContent(
          comments[i].querySelector(".bz_comment_number"))
          .replace(/[^\d]+/g, '');
      var class_ = '';
      if (/bz_closed/i.test(link.className + " "
          + link.parentNode.className)) {
        class_ += 'bz_closed ';
      }
      if (link.parentNode.tagName.toLowerCase() == 'i') {
        class_ += 'bztw_unconfirmed ';
      }
      var added = '<a href="show_bug.cgi?id=' + bugID
          + '" title="' + htmlEncode(title) + '" name="c'
          + number + '" class="' + class_ + '">' + bugID
          + '</a>';

      // inject the table row
      var reachedEnd = false;
      for (; row < rows.length; ++row) {
        var cells = rows[row].querySelectorAll("td");
        if (cells.length != 5)
          continue;
        if (time > trimContent(cells[1])) {
          if (row < rows.length - 1) {
            continue;
          }
          else {
            reachedEnd = true;
          }
        }
        if (time == trimContent(cells[1])) {
          cells[0].rowSpan++;
          cells[1].rowSpan++;
          var rowContents = [
              what, removed, added
          ];
          var tr = histDoc.createElement("tr");
          rowContents.forEach(function(cellContents) {
            var td = histDoc.createElement("td");
            td.innerHTML = cellContents;
            tr.appendChild(td);
          });
          if (row != rows.length - 1) {
            rows[row].parentNode.insertBefore(tr, rows[row + 1]);
          }
          else {
            rows[row].parentNode.appendChild(tr);
          }
        }
        else {
          var rowContents = [
              email, time, what, removed, added
          ];
          var tr = histDoc.createElement("tr");
          rowContents.forEach(function(cellContents) {
            var td = histDoc.createElement("td");
            td.innerHTML = cellContents;
            tr.appendChild(td);
          });
          if (reachedEnd) {
            rows[row].parentNode.appendChild(tr);
          }
          else {
            rows[row].parentNode.insertBefore(tr, rows[row]);
          }
        }
        break;
      }

      // remove the comment from the main doc
      comments[i].parentNode.removeChild(comments[i]);
    }
  }
}
