/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Bugzilla Tweaks.
 *
 * The Initial Developer of the Original Code is Mozilla Foundation.
 * Portions created by the Initial Developer are Copyright (C) 2010
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   Johnathan Nightingale <johnath@mozilla.com>
 *   Ehsan Akhgari <ehsan@mozilla.com>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

var reAttachmentType = /,\s+([^ )]*)[;)]/;

function viewAttachmentSource(attachments) {
  attachments.forEach(function(att) {
    if (!att.id) {
      return;
    }
    var typeName = att.mimeType;
    var elem = att.element;
    var id = att.id;
    var attachHref = elem.getAttribute("href");

    if (typeName == "application/java-archive"
        || typeName == "application/x-jar") {
      // Due to the fix for bug 369814, only zip files with this special
      // mime type can be used with the jar: protocol.
      // http://hg.mozilla.org/mozilla-central/rev/be54f6bb9e1e
      // signature of addLink is   addLink(elem, title, href)
      //
      // https://bugzilla.mozilla.org/show_bug.cgi?id=369814#c5 has more
      // possible mime types for zips?
      createDeadLink("viewSourceJAR_" + id, "JAR Contents",
          elem, "jar:" + attachHref + "!/", [], "pipe", null,
          null);
    }
    else if (typeName == "application/zip"
        || typeName == "application/x-zip-compressed"
        || typeName == "application/x-xpinstall") {
      createDeadLink("viewSourceZIP_" + id,
          "Static ZIP Contents", elem, "jar:" + attachHref
              + "!/", [], "pipe", null, null);
    }
    else if (typeName != "text/plain" && typeName != "patch" &&
    // Other types that Gecko displays like text/plain
    // http://mxr.mozilla.org/mozilla-central/source/parser/htmlparser/public/nsIParser.h
    typeName != "text/css" && typeName != "text/javascript"
        && typeName != "text/ecmascript"
        && typeName != "application/javascript"
        && typeName != "application/ecmascript"
        && typeName != "application/x-javascript" &&
        // Binary image types for which the "source" is not useful
        typeName != "image/gif" && typeName != "image/png"
        && typeName != "image/jpeg") {
      createDeadLink("viewSourcePlain_" + id, "Source", elem,
          "view-source:" + attachHref, [], "pipe", null, null);
    }
  })
}
