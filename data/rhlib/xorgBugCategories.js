// Released under the MIT/X11 license
// http://www.opensource.org/licenses/mit-license.php
"use strict";

/**
 * Returns true if the bug is in a good shape
 *
 * @return Boolean if the bug is either not in the category where we care about
 *         it (i.e., we don't have set up categories for this component) or if
 *         it is in the concerned categories, then it has a category recorded in
 *         the whiteboard input box.
 *
 */
function hasXorgBugsCategory() {
  var catRE = /\s*\[cat:.*?\]\s*/; // RE for testing whether
  // there is already category tag in the Whiteboard

  var isXOrgBug = filterByRegexp(
      constantData.xorgBugsCategories, getComponent());
  var whiteboardContent = document
      .getElementById("status_whiteboard").value;

  if (isXOrgBug) { // is it XOR?
    return catRE.test(whiteboardContent);
  }
  else {
    return true;
  }
}

/**
 * Create a category list to the upper toolbar
 */
function makeBugCategoriesList(catList) {
  var catRE = /\s*\[cat:.*?\]\s*/; // RE for testing whether
  // there is already category tag in the Whiteboard

  // Create <select> element and add it first blank <option>
  var targetDiv = document.getElementById("commit_top").parentNode;
  var categoryList = document.createElement("select");
  categoryList.setAttribute("id", "xorgBugsCategoriesSelect");
  categoryList.setAttribute("name", "xorgBugsCategoriesSelect");
  var optionElement = document.createElement("option");
  optionElement.value = null;
  optionElement.setAttribute("id", "catId_blank");
  optionElement.appendChild(document.createTextNode("---"));
  categoryList.appendChild(optionElement);

  // Fill-in <select> with <options>s for each category one
  if (catList) {
    catList.forEach(function(cat) {
      optionElement = document.createElement("option");
      optionElement.value = cat;
      optionElement.setAttribute("id", "catId_"
          + cat.replace(" ", "").toLowerCase());
      optionElement.appendChild(document.createTextNode(cat));
      categoryList.appendChild(optionElement);
    });
  }

  categoryList.addEventListener("change", function(evt) {
    var selectedCategory = "[cat:" + this.value + "]";
    var whiteboardElement = document
        .getElementById("status_whiteboard");

    if (hasXorgBugsCategory()) {
      whiteboardElement.value = whiteboardElement.value.replace(
          catRE, "");
    }
    addStuffToTextBox("status_whiteboard", selectedCategory);
  }, false);

  targetDiv.insertBefore(categoryList, targetDiv.firstChild);
}
