// Released under the MIT/X11 license
// http://www.opensource.org/licenses/mit-license.php

// For identification of graphics card
var manuChipStrs = [ [ "ATI Radeon", "ATI", "1002" ],
                     [ "ATI Mobility Radeon", "ATI", "1002" ],
                     [ "Intel Corporation", "INTEL", "8086" ], [ "NVIDIA", "NV", "10de" ] ];

// http://en.wikipedia.org/wiki/HSL_color_space
// when only the value of S is changed
// stupido!!! the string is value in hex for each color
var RHColor = new Color(158, 41, 43); // RGB 158, 41, 43; HSL 359, 1, 39
var FedoraColor = new Color(0, 40, 103); // RGB 0, 40, 103; HSL 359, 1, 39
var RawhideColor = new Color(0, 119, 0); // or "green", or RGB 0, 119, 0, or
// HSL
// 120, 0, 23
var RHITColor = new Color(102, 0, 102); // RGB 102, 0, 102; HSL 300, 0, 20
var titleParsedAttachment = "Part of the thread where crash happened";

// [ 126.386] (--) NOUVEAU(0): Chipset: "NVIDIA NVaf"
var logAnalyzeLogic = {
    "AnalyzeInterestingLine": {
      /*
       * [ 126.378] (--) PCI:*(0:4:0:0) 10de:08a0:106b:00c2 rev 162, Mem @
       * 0xd2000000/16777216, \ 0xc0000000/268435456, 0xd0000000/33554432, I/O @
       * 0x00001000/128, BIOS @ 0x????????/131072
       */
      re: [
        "^(\\[[ .0-9]+\\])?\\s*\\(--\\) PCI:\\*\\([0-9:]+\\)\\s*" +
        "([0-9a-f:]+).*$",
        "^\\s*\\[?[ 0-9.]*\\]?\\s*\\(--\\) "+
        "([A-Za-z]+)\\([0-9]?\\): Chipset: (.*)$",
      ],
      func: chipsetMagic
    },
    "AnalyzeXorgLogBacktrace": {
      /*
       * [ 126.385] (WW) Falling back to old probe method for vesa [ 126.385] (WW)
       * Falling back to old probe method for fbdev [ 126.386] (--) NOUVEAU(0):
       * Chipset: "NVIDIA NVaf" Backtrace: [ 33.158] Kernel command line: ro
       * root=LABEL=root rd_NO_LUKS rd_NO_LVM rd_NO_MD rd_NO_DM LANG=en_US.UTF-8
       * SYSFONT=latarcyrheb-sun16 KEYTABLE=us drm.debug=0x04
       *
       */
      re:  "^\\s*(\\[[0-9 .]*\\])?\\s*(\\((EE|WW)\\)|" +
        ".* [cC]hipset:.*)|\\s*(Backtrace|Kernel command line)",
      func: displayResultsInPanel
    },
    "AnalyzeDmesgErrors": {
      /*
       */
      re: "[fF]ail|[eE]rror|drm",
      func: displayResultsInPanel
    }
};

var ProfessionalProducts = [
                            "Red Hat Enterprise Linux",
                            "Red Hat Enterprise MRG"
                            ];

// END OF CONSTANTS

var btSnippet = null;
var localThings = null; // I don't like it, but we need to store it somewhere for now

function RHOnMessageHandler(msg, nextHandlerList) {
  switch (msg.cmd) {
  case "Error":
    alert("Error " + msg.data);
    break;
  case "Unhandled":
    break;
  case "queryUpstream":
    queryUpstreamCallback(msg.data, constantData.queryUpstreamBug);
    break;
  default:
    if (nextHandlerList) {
      var nextHandler = nextHandlerList.splice(0, 1);
      if (nextHandler[0]) {
        nextHandler[0](msg, nextHandlerList);
      }
    }
    else {
      console.error("Error: unknown RPC call " + msg.toSource());
    }
  break;
  }
}

// RHBugzillaPage object

/**
 * Auxiliary function to compute more complicated resolution
 */
function closeSomeRelease() {
  // for RAWHIDE close as RAWHIDE,
  // if active selection -> CURRENTRELEASE
  // and put the release version to
  // "Fixed in Version" textbox
  // otherwise -> NEXTRELEASE
  selectOption("bug_status", "CLOSED");
  var text = getSel();
  var resolution = "";

  if (text.length > 0) {
    resolution = "CURRENTRELEASE";
    document.getElementById("cf_fixed_in").value = text;
  }
  else if (document.getElementById("version").value === "rawhide") {
    resolution = "RAWHIDE";
  }
  else {
    resolution = "NEXTRELEASE";
  }
  centralCommandDispatch("resolution", resolution);
}

/**
 * Additional commands specific for this subclass, overriding superclass one.
 */
function RHcentralCommandDispatch(cmdLabel, cmdParams) {
  switch (cmdLabel) {
  // Set up our own commands
  case "closeUpstream":
    addClosingUpstream();
    break;
  case "computeResolution":
    closeSomeRelease();
    break;
  case "queryStringUpstreamBugzilla":
    queryUpstream(constantData.queryUpstreamBug);
    break;
  case "sendBugUpstream":
    sendBugUpstream();
    break;
  case "markTriaged":
    markBugTriaged();
    break;
  case "chipMagic":
    fillInWhiteBoard(cmdParams);
    break;
  case "addExternalBugID":
    addExternalBug();
    break;
  // If we don't have it here, call superclass method
  default:
    if (MozCentralCommandDispatch) {
      MozCentralCommandDispatch(cmdLabel, cmdParams);
    }
    else {
      console.error("Unknown command:\n" + cmdLabel + "\nparameters:\n" + cmdParams);
    }
  break;
  }
}

/* === Bugzilla functions === */

/**
 * Open a tab in the upstream bugzilla to create a new bug
 *
 * @return none
 */
function sendBugUpstream(thgs) {
  if (thgs) {
    localThings = thgs;
    return;
  }
  if (localThings) {
    myDebug('sendBugUpstream: creating new bug for component ' +
        getComponent());
    var admitMsg = "(originally filed as " + window.location.href + ")\n\n";
    var urlStr = filterByRegexp(constantData.newUpstreamBug, getComponent());
    myDebug('sendBugUpstream: urlStr = ' + urlStr);
    if (!urlStr) {
      return null;
    }

    self.postMessage(new Message("OpenBugUpstream", {
      url: urlStr,
      subject: document.getElementById("short_desc_nonedit_display").
        textContent.trim(),
      comment: admitMsg + localThings.comments.getAllCommentsText()
    }));

    return true;
  }
}


/**
 * Given line to be parsed, find out which chipset it is and fill in the
 * whiteboard
 *
 * @param PCIidArrObj
 *          object with two fields id Array manufacturer-ID and product-ID (PCI
 *          IDs) chipsetLine whole line containing PCI ID.
 * @param driverStr
 *          String with the driver name
 * @return None
 *
 */
function fillInWhiteBoard(cardName) {
  clickMouse("editme_action");
  var titleElem = document.getElementById('short_desc');
  titleElem.value = '[' + cardName + ']\u00A0' + titleElem.value;
  document.getElementById("fillin_btn").style.display = "none";
}

/**
 * Creates a button to change summary by adding a graphic chip label
 *
 * @param Array
 *          with matching results of re.exec()
 */
function chipsetMagic (interestingLineArr) {
  // parse Xorg.0.log
  var cardStr = "";

  if (interestingLineArr.length >0) {
    var interestingArray = interestingLineArr[0];
    if (interestingArray.length > 1) {
      var interestingPCIID = interestingArray[2].trim().split(":");
      // If we have Chipset line, we should parse it as well and
      // add to the button
      if (interestingPCIID.length > 1) {
        var PCIid = (interestingPCIID[0] + ":" + interestingPCIID[1]).
        toUpperCase();
        // Nvidia driver provides good code in the Chipset line
        if (interestingPCIID[0].toLowerCase() == "10de") {
          cardStr = interestingLineArr[1][2].
          replace(/\s*nvidia\s*/ig,"").
          replace('"','','g');
        } else {
          try {
            cardStr = constantData.chipNames[PCIid][0];
          } catch (e if e instanceof TypeError)  {
            PCIid = PCIid.toLowerCase();
            cardStr = null;
            alert("PCI ID " + PCIid + " is not known!");
            self.postMessage(new Message("SetClipboard", PCIid.toString()));
          } catch (e) {
            throw e;
          }
        }
      }
      else {
        cardStr = null;
      }

      if (cardStr) {
        createNewButton("short_desc_nonedit_display", false, {
          "name": "Fill In",
          "chipMagic": cardStr
        });
      }
    }
  }
}

function displayResultsInPanel(results) {
  var innerString = "";

  if (results.length > 0) {
    results.splice(0, 1); // remove headers
    results.sort();

    results.forEach(function(lRE) {
      innerString += lRE.input + "<br>\n";
    });
    innerString += "----------<br>\n" +
    results.length + " interesting lines found.";
  }
  else {
    innerString += "No matching lines found!";
  }

  self.postMessage(new Message("OpenURLinPanel",
      'data:text/html;charset=utf-8,' +
      '<!DOCTYPE html PUBLIC "-//IETF//DTD HTML 2.0//EN">' +
      "<html><head><title>Xorg.0.log analysis</title></head><body><pre>\n" +
      innerString.trim() +
  "\n</pre></body></html>"));
}

function findInterestingLine(wholeLog, backMsg) {
  var REstr = logAnalyzeLogic[backMsg].re;
  var REarr = [];
  if (typeof REstr == "string") {
    REarr = [new RegExp(REstr)];
  }
  else if (Array.isArray(REstr)) {
    REarr = REstr.map(function (reone) {
      return new RegExp(reone);
    });
  }

  var results = [];
  wholeLog.split("\n").
  forEach(function(line) {
    REarr.forEach(function (re, reIdx) {
      if (re.test(line)) {
        results.push(re.exec(line));
      }
    });
  });
  logAnalyzeLogic[backMsg].func(results);
}


/**
 * Get attached Xorg.0.log, parse it and find the value of chip. Does not fill
 * the whiteboard itself, just adds button to do so,paramList so that slow
 * XMLHttpRequest is done in advance.
 *
 * @param log
 *          array of XorgLogAttList
 * @return None
 */
function analyzeXorgLog(attachID, backMsg) {
  var req = new XMLHttpRequest();
  req.open("GET",  "/attachment.cgi?id=" + attachID, true);
  req.onreadystatechange = function (aEvt) {
    if (req.readyState == 4) {
       if(req.status == 200) {
         findInterestingLine(req.responseText, backMsg);
       }
       else
         console.error('Error', req.statusText);
    }
  };
  req.send(null);
}

/**
 * Add an external bug reference from the input box
 *
 * @return String with the external bug URL or null
 */
function addExternalBug() {
  // that's a bad id, if there is a one. :)
  var inputBox = document.getElementById("external_bug_id");
  var externalBugID = 0;
  var wholeURL = "";

  // Fix missing ID on the external_id SELECT
  document.getElementsByName("external_id")[0].
    setAttribute("id", "external_id");

  if (inputBox.value.match(/^http.*/)) {
    wholeURL = inputBox.value;
    // TODO this is probably midlly ineffective ... we do parseURL here,
    // and getBugNoFromURL does it as well.
    externalBugID = getBugNoFromURL(wholeURL);
    if (externalBugID) {
      inputBox.value = externalBugID;
    }
    // get bugzillaName and set the label
    var bugzillaName = getBugzillaName(parseURL(wholeURL).host,
      constantData.bugzillaLabelNames);
    selectOptionByLabel("external_id", bugzillaName);
    return wholeURL;
  }
  else if (!isNaN(inputBox.value)) {
    return document.getElementById("external_id").value +
      "show_bug.cgi?id=" + parseInt(inputBox.value, 10);
  }
  else {
    // no inputBox.value -- maybe there is an external bug from
    // the previous commit?
    return null;
  }
}

/**
 * Add information about the upstream bug upstream, and closing it.
 *
 * @param evt
 *          Event which called this handler
 * @return none
 */
function addClosingUpstream() {

  var newBugURL = addExternalBug();

  if (!newBugURL) {
    console.warning("No external bug specified among the External References!");
    return null;
  }

  var msgStr = constantData.commentStrings.sentUpstreamString;
  msgStr = msgStr.replace("§§§", newBugURL);
  centralCommandDispatch("comment",msgStr);
  centralCommandDispatch("status", "CLOSED");
  centralCommandDispatch("resolution", "UPSTREAM");

  return ;
}

/**
 *
 */
function parseBacktrace (ret) {
  var signalHandler = new RegExp("^\\s*#[0-9]*\\s*<signal handler called>");
  var frameNo = new RegExp("^\\s*#([0-9]*)\\s");

  var splitArray = ret.split("\n");
  var i = 0, ii = splitArray.length;
  var outStr = "", curLine = "", numStr = "";
  var lineCounter = 0, endLineNo = 0;

  // TODO shouldn't we just cut off and analyze whole thread?
  while (i < ii) {
    if (signalHandler.test(splitArray[i])) {
      break;
    }
    i++;
  }

  if (i < ii) {
    lineCounter = parseInt(frameNo.exec(splitArray[i])[1], 10);
    endLineNo = lineCounter + NumberOfFrames;
    curLine = splitArray[i];
    while ((lineCounter < endLineNo) && (curLine.trim().length > 0)
        && (i < ii)) {
      outStr += curLine + '\n';
      numStr = frameNo.exec(curLine);
      if (numStr) {
        lineCounter = parseInt(numStr[1], 10);
      }
      i++;
      curLine = splitArray[i];
    }
    return outStr;
  }
  return "";
}

function RHBZinit(things) {
  // inheritance ... call superobject's constructor
  var AbrtRE = new RegExp("^\\s*\\[abrt\\]");
  var btSnippet = "";
  sendBugUpstream(things);

  var chipMagicInterestingLine = "";

  // getBadAttachments
  var XorgLogAttList = [];
  var XorgLogAttListIndex = 0;
  things.attachments.markBadAttachments();

  var parsedAttachments = things.attachments.getParsedAttachments();

  if (constantData.defaultAssignee) {
    setDefaultAssignee();
  }

  if (constantData.xorgBugsCategories) {
    var XBZlist = filterByRegexp(constantData.
        xorgBugsCategories, getComponent());
    if (XBZlist) {
      makeBugCategoriesList(XBZlist);
    }
  }

  // setup logging only when we ask for it
  if (config.submitsLogging && (window.location.hostname == "bugzilla.redhat.com")) {
    setUpLogging();
  }

  // Dig out backtrace protection against double-firing?
  btSnippet = "";

  if (config.parseAbrtBacktraces && AbrtRE.test(getSummary())) {
    pasteBacktraceInComments(parsedAttachments);
  }

  // Just add a link to every Xorg.0.log link analyzing it.
  things.attachments.addCheckXorgLogLink();

  setBranding(things);

  // Don't allow to submit a page which would change the bug to 0xFFFF component
  document.forms.namedItem("changeform").addEventListener(
      "submit", function(aEvt) {
        if (getComponent() == "0xFFFF") {
          alert("Don't change the component to 0xFFFF");
          aEvt.stopPropagation();
          aEvt.preventDefault();
        }
      }, false);

  // Uncheck "set default assignee" when the assignee is changed by other means
  var assAnchor = document.getElementById("assigned_to");
  if (assAnchor) {
    assAnchor.addEventListener("change",
        function() {
      changeAssignee(null);
    }, false);
  };
};
