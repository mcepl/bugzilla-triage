//Released under the MIT/X11 license
//http://www.opensource.org/licenses/mit-license.php

var reqCounter = 0; // TODO should be probably a dict indexed by called method

/**
 * Callback function for the XMLRPC request
 *
 * @param ret
 *          Object with xmlhttprequest response with attributes: + status -- int
 *          return code + statusText + responseHeaders + responseText
 */
function XMLRPCcallback() {
  reqCounter--;
  if (reqCounter <= 0) {
    setTimeout(function() {
      window.location.reload(true);
    }, 1000);
  }
}

/**
 * The worker function -- call XMLRPC to fix MIME type of the particular
 * attachment
 *
 * @param id
 *          Integer with the attachment id to be fixed
 * @param type
 *          String with the new MIME type, optional defaults to "text/plain"
 * @param email
 *          Boolean whether email should be sent to appropriate person; option,
 *          defaults to false
 *
 * updateAttachMimeType($data_ref, $username, $password)
 *
 * Update the attachment mime type of an attachment. The first argument is a
 * data hash containing information on the new MIME type and the attachment id
 * that you want to act on.
 *
 * $data_ref = { "attach_id" => "<Attachment ID>", # Attachment ID to perform
 * MIME type change on. "mime_type" => "<New MIME Type Value>", # Legal MIME
 * type value that you want to change the attachment to. "nomail" => 0, #
 * OPTIONAL Flag that is either 1 or 0 if you want email to be sent or not for
 * this change };
 *
 */
function fixAttachById(bugId, id, type, email) {
  myDebug("bugId = " + bugId + ", id = " + id);
  if (type === undefined) {
    type = "text/plain";
  }
  if (email === undefined) {
    email = 0;
  }

  // Logic of nomail is negative ... turn around
  email = !email;

  if (email === true) {
    email = 1;
  }
  else {
    email = 0;
  }

  // https://bugzilla.redhat.com/docs/en/html/api/extensions/RedHat\
  //     /lib/WebService/Bugzilla.html
  // test on https://bugzilla.redhat.com/show_bug.cgi?id=485145
  var params = {
    'id': bugId,
    'attach_id' : id,
    'mime_type' : type,
    'nomail' : email
  };

  makeJSONRPCCall("RedHat.updateAttachMimeType", params, XMLRPCcallback);
  reqCounter++;
}

/**
 * Add a link to the bad attachment for fixing it.
 *
 * @param
 * <TR> DOM jQuery element with a bad attachment
 * @return none
 */
function addTextLink(att) {
  var elemS = att.element.getElementsByTagName("td");
  var elem = elemS[elemS.length - 1];
  myDebug("att = " + att.toSource());
  createDeadLink("addFix2TextLink", "text", elem, fixAttachById,
      [ att.bugId, att.id ], "br");
}
