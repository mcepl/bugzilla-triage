//Released under the MIT/X11 license
//http://www.opensource.org/licenses/mit-license.php
"use strict";

// FIXME resp is JSON, not XML anymore
function addAttachmentCallback(resp) {
  var newAttachID = parseInt(
      resp.params.param.value.array.ids.value.int, 10);
  // FIXME callback.call(param, newAttachID, data.length);
}

/**
 *
 * This has to stay in RHBugzillaPage because upstream doesn't have
 * addAttachment XML-RPC call yet.
 *
 Params: $params = {
 id => '<bug_id>', # ID of the bug report
 comment => "<Attachment Comment>", # OPTIONAL Text string containing comment to add.
 description => "<Attachment Description>", # REQUIRED Text Description of the attachment.
 isprivate => <Private Attachment ON/OFF 1/0>, # OPTIONAL Whether the Attachment
 will be private # Default: false
 filename => "<Attachment Filename>", REQUIRED The name of the file to attach to the bug report.
 obsoletes => [List of attach_id's to obsolete], OPTIONAL List if attachment ids that are
 obsoleted by this new attachment.
 ispatch => <Patch Attachment True/False 1/0>, OPTIONAL Whether the attachment is a Patch
 or not, if not provided the it will be considered NON Patch attachment.
 contenttype => "<Attachment Content Type>", OPTIONAL If the attachment is patch
 REQUIRED If the attachment is not a patch
 If the attachment is patch then contenttype will always be text/plain
 data => "<Encoded String of the Attachment Data>", REQUIRED It is a base64
 encoded string of the actual attachment data.
 nomail => 0, OPTIONAL Flag that is either 1 or 0 if you want email
 to be send ot not for this change }
 */
function addAttachment(data, callback, param) {
  var params = [];

  if (!constantData.passwordState.passAvailable) {
    // No password for XML-RPC service, no XML-RPC service, so simple!
    return null;
  }

  var params = {
    ids : [getBugNo()],
    summary : titleParsedAttachment,
    file_name : "parsed-backtrace.txt",
    content_type : "text/plain",
    data : window.btoa(data)
  };

  makeJSONRPCCall("Bug.add_attachment", params, addAttachmentCallback);
  reqCounter++;
}
