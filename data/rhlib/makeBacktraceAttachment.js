//Released under the MIT/X11 license
//http://www.opensource.org/licenses/mit-license.php

/**
 *
 */
function pasteBacktraceInComments(atts) {
  /*
   * Let's comment it out, and we'll see if anything breaks. TODO This paragraph
   * looks suspicous ... what is it? Does it belong to this function? var
   * notedLabel = document.querySelector("label[for='newcc']"); while
   * (notedLabel.firstChild) { var node =
   * notedLabel.removeChild(notedLabel.firstChild);
   * notedLabel.parentNode.insertBefore(node, notedLabel); }
   * notedLabel.parentNode.removeChild(notedLabel);
   */

  // FIXME BROKEN and its depending functions are even more broken
  return null;

  var abrtQueryURL = "https://bugzilla.redhat.com/buglist.cgi?"
    + "cmdtype=dorem&remaction=run&namedcmd=all%20NEW%20abrt%20crashes&"
    + "sharer_id=74116";

  var mainTitle = document
  .getElementsByClassName("bz_alias_short_desc_container")[0];

  createDeadLink("callAbrtQuery_link", "Abrt bugs", mainTitle,
      abrtQueryURL, [], false, null, "a");

  if (idContainsWord("cf_devel_whiteboard", 'btparsed')) {
    addStuffToTextBox('status_whiteboard', 'btparsed');
  }

  if (!(isTriaged()
      || idContainsWord("status_whiteboard", 'btparsed') || (atts.length > 0))) {
    var btAttachments = atts.filter(function(att) {
      return (/File: backtrace/.test(att[0]));
    });
    // TODO we need to go through all backtrace attachments, but
    // just the first one will do for now, we would need to do async
    //
    btAttachments
    .forEach(
        function(x) {
          var attURL = "https://bugzilla.redhat.com/attachment.cgi?id="
            + x[1];
          if ((!btSnippet) && // ???? FIXME
              (!idContainsWord("status_whiteboard", 'btparsed'))) {
            Request({
              url : attURL,
              onComplete : function(response) {
                if (response.status == 200) {
                  btSnippet = parseBacktrace(response.text);
                  if (btSnippet) {
                    addCheckShowLink(x, btSnippet);
                  }
                }
              }
            }).get();
          }
        }, this);
  }
  // Add "show BT" links
  if (parsedAttachments.length > 0) {
    this.parsedAttachments.forEach(function(att) {
      addShowParsedBTLink(att);
    });
  }
}

/**
 * Open new window with the content of the attachment.
 *
 * @param id
 *          Number of the attachment id
 * @return none
 */
function showAttachment(id) {
  self.postMessage(new Message("OpenURLinPanel", "https://"
      + window.location.hostname + "/attachment.cgi?id=" + id));
}

/**
 * add a link opening a window with the parsed backtrace
 *
 * @param att
 *          Attachment object
 */
function addShowParsedBTLink(att) {
  var elem = att[4].querySelector("td:last-of-type");
  createDeadLink("showParsedBacktraceWindow-" + att[1],
      "showBT", elem, showAttachment, att[1], true);
}

/**
 * Unfinished ... see above FIXME BROKEN AND DOESN'T WORK
 */
function addNewAttachmentRow(origAtt, newAttId, newAttSize) {
  var that = this;
  var oldAddBTLink = document
  .getElementById("attachBacktraceActivator");
  oldAddBTLink.parentNode.removeChild(oldAddBTLink);
  var newTRElem = origAtt[4].cloneNode(true);

  // fix number of the attachment
  Array.forEach(newTRElem.getElementsByTagName("a"), function(
      aEl) {
    aEl.setAttribute("href", aEl.getAttribute("href").replace(
        origAtt[1], newAttId));
  });

  var aElements = newTRElem.getElementsByTagName("a");
  aElements[0].setAttribute("name", "parsed-backtrace.txt");
  aElements[0].getElementsByTagName("b")[0].textContent = titleParsedAttachment;

  var sizeSpan = newTRElem
  .getElementsByClassName("bz_attach_extra_info")[0];
  sizeSpan.textContent = "(" + (newAttSize / 1024).toFixed(2)
  + " KB, text/plain)";

  // aElements[1].textContent = new Date().toString(); TODO we should add
  // eventually, but not pressing

  var vcardSpan = newTRElem.getElementsByClassName("vcard")[0];
  if (vcardSpan !== undefined) {
    var vcardSpanClassList = vcardSpan.classList;
    if (/@redhat\.com/.test(this.login)
        && !vcardSpanClassList.contains("redhat_user")) {
      vcardSpanClassList.add("redhat_user");
    }
    var vcardAElem = vcardSpan.getElementsByTagName("a")[0];
    vcardAElem.setAttribute("title", this.login);
    vcardAElem.setAttribute("href", "mailto:" + this.login);
    vcardAElem.className = "email";
    vcardAElem.innerHTML = "<span class='fn'>" + this.login
    + "</span>";
  }

  var elem = newTRElem.querySelector("td:last-of-type");
  this.createDeadLink("showBacktrace", "show BT", elem,
      this.showAttachment, newAttId, false);

  origAtt[4].parentNode.insertBefore(newTRElem,
      origAtt[4].nextSibling);
}

/**
 * Add a link to create a new attachment with a parsed backtrace
 *
 * @param oldAtt
 *          Object with an attachment row
 * @param snippet
 *          String with parsed backtrace
 * @return none
 */
function addCheckShowLink(oldAtt, snippet) {
  var elem = oldAtt[4].querySelector("td:last-of-type");
  /*
   * createDeadLink("attachBacktraceActivator", "add parsed BT", elem,
   * function(x) { // pass function and parameters as two separate parameters,
   * the function to be called from // addAttachment addAttachment(snippet,
   * addNewAttachmentRow, oldAtt); }, [], true);
   */
}
