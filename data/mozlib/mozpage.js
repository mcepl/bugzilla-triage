// Released under the MIT/X11 license
// http://www.opensource.org/licenses/mit-license.php

/**
 * Abstract model of flags found on BMO (and not only there)
 *
 * @return Object with function properties:
 *   - set Approve flag (currently "+"),
 *   - reject Reject flag (currently "-"),
 *   - ask ask for decision (currently "?"),
 *   - unset clear the flag to the initial state (currently "--"), and
 *   - dump dump internal variable to console
 */
var mozFlags = (function() {
  var flags = {};

  function _init() {
    var tdColumn2 = document.getElementById("bz_show_bug_column_2");
    if (tdColumn2) { // TODO we should really make some configuration for non-Mozilla
      var flag_selects = tdColumn2.querySelectorAll("td.field_value select");
      Array.forEach(flag_selects, function(sel) {
        var label = tdColumn2.querySelector("label[for='" + sel.id + "']");
        if (label) {
          var key = label.textContent.trim().replace(/\s*:?$/,"");
          flags[key] = sel.id;
        }
      });
    }
  }

  function _setFlag(label) {
    selectOption(flags[label], "+");
  }

  function _rejectFlag(label) {
    selectOption(flags[label], "-");
  }

  function _askFlag(label) {
    selectOption(flags[label], "?");
  }

  function _unsetFlag(label) {
    selectOption(flags[label], "--");
  }

  function _dumpFlags() {
    console.log("collected flags are " + flags.toSource());
  }

  _init();
  return {
    set: _setFlag,
    reject: _rejectFlag,
    ask: _askFlag,
    unset: _unsetFlag,
    dump: _dumpFlags
  };

})();

// Currently empty message handler
function MozOnMessageHandler(msg, nextHandlerList) {
  switch (msg.cmd) {
  default:
    if (nextHandlerList) {
      var nextHandler = nextHandlerList.splice(0, 1);
      if (nextHandler[0]) {
        nextHandler[0](msg, nextHandlerList);
      }
    }
    else {
      console.error("Error: unknown RPC call " + msg.toSource());
    }
  break;
  }
}

/**
 * Additional commands specific for this subclass, overriding superclass one.
 */
function MozCentralCommandDispatch(cmdLabel, cmdParams) {
  switch (cmdLabel) {
  // Set up our own commands
  case "setFlag":
    mozFlags.set(cmdParams);
    break;
  case "unsetFlag":
    mozFlags.reject(cmdParams);
    break;
  case "askFlag":
    mozFlags.ask(cmdParams);
    break;
  case "clearFlag":
    mozFlags.unset(cmdParams);
    break;
  default:
    console.error("Unknown command:\n" + cmdLabel + "\nparameters:\n" + cmdParams);
  break;
  }
}
