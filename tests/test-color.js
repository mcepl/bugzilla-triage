/* global exports: false, require: false */
// TODO: add some failing tests as well
"use strict";
// var util = require("color");

// testing Color object
var ensureColorNew = function(test) {
  var col = new util.Color(255, 255, 166);
  test.assertEqual(col.toString(), "#ffffa6",
      "creation of new RGB Color object");
};

var ensureColorUpdate = function(test) {
  var col = new util.Color(255, 255, 166);
  col.update(255, 224, 176);
  test.assertEqual(col.toString(), "#ffe0b0",
      "updating Color object");
};

var ensureColorHSL = function(test) {
  var col = new util.Color(255, 224, 176);
  test.assertEqual(col.hsl().toSource(),
      "[0.10126582278481013, 1, 0.8450980392156863]",
      "converting to HSL model");
};

var ensureColorLight = function(test) {
  var col = new util.Color(255, 224, 176);
  test.assertEqual(col.lightColor().toString(), "#e8dcc9",
      "getting a light color");
};
