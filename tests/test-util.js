/* global exports: false, require: false */
/* jslint plusplus: false */
// TODO: add some failing tests as well
"use strict";
var util = require("util");
var urlMod = require("url");

// testing util.heir
var ensureHeir = function(test) {
  var fedlimid = {}, naoise = {};

  function Father(x) {
    this.family = x;
  }

  Father.prototype.getFamily = function getFamily() {
    return this.family;
  };

  function Son(x, w) {
    Father.call(this, x);
    this.wife = w;
  }

  Son.prototype = util.heir(Father);
  Son.prototype.constructor = Son;

  Son.prototype.getWife = function getWife() {
    return this.wife;
  };

  Son.prototype.getFamily = function getFamily() {
    var upFamily = Father.prototype.getFamily.call(this);
    return upFamily + ", " + this.wife;
  };

  // for curious and non-Celtic
  // http://en.wikipedia.org/wiki/Deirdre :)
  fedlimid = new Father("mac Daill");
  naoise = new Son("Usnech", "Deirdre");

  test.assertEqual(fedlimid.getFamily(), "mac Daill",
      "checking creation of new simple object");

  test.assertEqual(naoise.getWife(), "Deirdre",
      "checking creation of new daughter object");

  test.assertEqual(naoise.getFamily(), "Usnech, Deirdre",
      "checking creation of new overloaded method");
};

// testing util.isInList
var ensureIsInListTrue = function(test) {
  test.assert(util.isInList("a", [
    "a"
  ]), "conversion of a string to an array");
};

var ensureIsInListFalse = function(test) {
  test.assert(!util.isInList("b", [
    "a"
  ]), "conversion of a string to an array");
};

var ensureIsInListEmpty = function(test) {
  test.assert(!util.isInList("b", []),
      "conversion of a string to an array");
};

var ensureIsInListNoMember = function(test) {
  test.assert(!util.isInList("", [
    "x"
  ]), "conversion of a string to an array");
};

// testing util.filterByRegexp
var ensureFilterByRegexp = function(test) {
  var list = [
      {
        "regexp" : "test(ing|ed)",
        "addr" : "correct"
      }, {
        "regexp" : "ba.*d",
        "addr" : true
      }
  ];

  test.assertEqual(util.filterByRegexp(list, "testing"),
      "correct", "simple testing of filterByRegexp");
  test.assertEqual(util.filterByRegexp(list, "unknown value"),
      "",
      "simple testing of filterByRegexp with non-found address");
  test
      .assert(util.filterByRegexp(list, "baaad"),
          "simple testing of filterByRegexp with non-string return value");
};

var ensureFilterByRegexpEmpty = function(test) {
  test.assertRaises(function() {
    util.filterByRegexp(undefined, "tralala");
  }, "list is undefined",
      "filterByRegexp throws an exception with empty list");
};

// testing util.getISODate
var ensureGetISODate = function(test) {
  test.assertEqual(util
      .getISODate("Mon May 31 2010 23:29:09 GMT+0200 (CET)"),
      "2010-05-31",
      "conversion of a Date to ISO-formatted String");
};

// testing util.valToArray
var ensureValToArrayString = function(test) {
  test.assertEqual(JSON.stringify(util.valToArray("a")), JSON
      .stringify([
        "a"
      ]), "conversion of a string to an array");
};

var ensureValToArrayEmpty = function(test) {
  test.assertEqual(JSON.stringify(util.valToArray("")), JSON
      .stringify([
        ""
      ]), "conversion of an empty string to an array");
};

var ensureValToArrayArray = function(test) {
  test.assertEqual(JSON.stringify(util.valToArray([
    "a"
  ])), JSON.stringify([
    "a"
  ]), "non-conversion of an array");
};

// testing util.addCSVValue
var ensureCSVAddedToNull = function(test) {
  test.assertEqual(util.addCSVValue("", "b"), "b",
      "adding a string to empty string");
};

var ensureCSVAddedNull = function(test) {
  test.assertEqual(util.addCSVValue("a", ""), "a",
      "adding nothing to a string");
};

var ensureCSVAddedString = function(test) {
  test.assertEqual(util.addCSVValue("a", "b"), "a, b",
      "adding one string to another one");
};

var ensureCSVAddedArray = function(test) {
  test.assertEqual(util.addCSVValue("a", [
      "b", "c"
  ]), "a, b, c", "adding array to a string");
};

var ensureCSVAddedArray2Array = function(test) {
  test.assertEqual(util.addCSVValue("a, b", [
      "c", "d"
  ]), "a, b, c, d", "adding one array to another");
};

// testing util.removeCSVValue
var ensureCSVRemoveSimple = function(test) {
  test.assertEqual(util.removeCSVValue("a, b", "b"), "a",
      "removing one string from an array");

};

// also checking a tolerancy against different ways of writing arrays
var ensureCSVRemoveNonMember = function(test) {
  test
      .assertEqual(util.removeCSVValue("a,b", "c"), "a, b",
          "removing a string from an array of which it isn't a member");

};

var ensureCSVRemoveEmpty = function(test) {
  test.assertEqual(util.removeCSVValue("", "c"), "",
      "removing a string from an empty array");

};

// testing util.getObjectKeys
var ensureGetObjectKeys = function(test) {
  var testObj = {
    a : 1,
    b : 2
  };
  test.assertEqual(JSON.stringify(util.getObjectKeys(testObj)),
      JSON.stringify([
          "a", "b"
      ]), "getting keys from a object");
};

// testing util.getParamsFromURL
var ensureGetParamsFromURL = function(test) {
  test
      .assertEqual(
          JSON
              .stringify(util
                  .getParamsFromURL("https://bugzilla.redhat.com/show_bug.cgi?id=549066")),
          JSON.stringify({
            id : "549066"
          }), "simply compare result of bugzilla show_page URL");
  var complexURL = new urlMod.URL(
      "http://www.google.com/search?ie=UTF-8&oe=UTF-8&sourceid=navclient"
          + "&gfns=1&q=gg+javascript+url+parse");
  test.assertEqual(JSON.stringify(util
      .getParamsFromURL(complexURL)), JSON.stringify({
    "ie" : "UTF-8",
    "oe" : "UTF-8",
    "sourceid" : "navclient",
    "gfns" : "1",
    "q" : "gg+javascript+url+parse"
  }), "simply compare result of bugzilla show_page URL");
  test.assertEqual(JSON.stringify(util
      .getParamsFromURL("https://bugzilla.redhat.com/")), JSON
      .stringify({}), "URL without any parameters");
  test.assertRaises(function() {
    util.getParamsFromURL("");
  }, "Missing URL value!", "No URL");
};

// testing util.getBugNo
var ensureGetBugNo = function(test) {
  var bugNo = util
      .getBugNo("https://bugzilla.redhat.com/show_bug.cgi?id=597141");
  test.assertEqual(bugNo, 597141, "getting bug number");
  bugNo = util
      .getBugNo("https://bugzilla.redhat.com/show_bug.cgi?id=serialWacom");
  test
      .assertEqual(bugNo, "serialWacom",
          "getting a bug alias; there is no guarantee of getting number!");
};
