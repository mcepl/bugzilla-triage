/* global exports: false, require: false */
/* jslint plusplus: false */
"use strict";
var xrpc = require("xmlrpc");
var xmlOut = "<?xml version=\"1.0\"?>\n"
    + "<methodCall>\n<methodName>bugzilla.updateAttachMimeType</methodName>\n"
    + "<params>\n<param>\n<value><struct>\n<member>\n<name>attach_id</name>\n"
    + "<value><string>myId</string></value>\n</member>\n<member>\n"
    + "<name>mime_type</name>\n<value><string>text/plain</string></value>\n</member>\n"
    + "<member>\n<name>nomail</name>\n<value><string>billg@microsoft.com</string>"
    + "</value>\n</member>\n</struct>\n</value>\n</param>\n<param>\n"
    + "<value><string>me@example.com</string></value>\n</param>\n"
    + "<param>\n<value><string>secret</string></value>\n</param>\n"
    + "<param>\n<value><double>3.14</double></value>\n</param>\n"
    + "<param>\n<value><boolean>1</boolean></value>\n</param>\n"
    + "</params>\n</methodCall>";

exports.ensureLeadingZero = function(test) {
  test.assert(typeof (xrpc.leadingZero) == "function");
  test.assertEqual(xrpc.leadingZero("1"), "01");
  test.assertEqual(xrpc.leadingZero(1), "01");
  test.assertEqual(xrpc.leadingZero("11"), "11");
  test.assertEqual(xrpc.leadingZero(11), "11");
  test.assertEqual(xrpc.leadingZero("111"), "111");
  test.assertEqual(xrpc.leadingZero(111), "111");
  test.assertEqual(xrpc.leadingZero("-1"), "-1");
  test.assertEqual(xrpc.leadingZero(-1), "-1");
  test.assertEqual(xrpc.leadingZero("zzz"), "zzz");
};

// testing xrpc.XMLRPCMessage
exports.ensureGenerateXMLRPC = function(test) {
  var msg = new xrpc.XMLRPCMessage(
      "bugzilla.updateAttachMimeType");
  msg.addParameter({
    'attach_id' : "myId",
    'mime_type' : "text/plain",
    'nomail' : "billg@microsoft.com"
  });
  msg.addParameter("me@example.com");
  msg.addParameter("secret");
  msg.addParameter(3.14);
  msg.addParameter(true);
  test
      .assertEqual(msg.xml(), xmlOut, "generate XML-RPC message");
};
