var MatchPattern = require("match-pattern").MatchPattern;

exports.ensureMatchPattern = function(test) {
  var pattern = new MatchPattern(
      "https://bugzilla.redhat.com/attachment.cgi");
  console.log("pattern = " + pattern);
  test.assert(pattern
      .test("https://bugzilla.redhat.com/attachment.cgi"),
      "testing match pattern");
  test
      .assert(
          !pattern
              .test("https://bugzilla.redhat.com/attachment.cgi?bugid=676538&action=enter"),
          "testing failing match pattern");
};
