/*jslint white: false, eqeqeq: false, plusplus: false, onevar: false, newcap: false */
/*global exports: false, require: false, console: false, log: false */
"use strict";
var Cc = require("chrome").Cc;
var Ci = require("chrome").Ci;
var main = require("main");
var utilMod = require("util");
var testPageMod = require("pagemod-test-helpers").testPageMod;
var self = require("self");

var theURL = main.theURL;
var testURL = self.data.url('tests/change-more-bugs01.html');
var JSONifiedMessage = '{"cmd":"testMessage","data":{"a":"first","b":"second"}}';

exports.ensureMessagesWork = function(test) {
  var msg = new utilMod.Message("testMessage", {
    a : "first",
    b : "second"
  });
  test.assertEqual(msg.cmd, "testMessage",
      "msg.cmd comes over well");
  test.assertEqual(msg.data.a, "first",
      "msg.data.a comes over well");
  test.assertEqual(msg.data.b, "second",
      "msg.data.b comes over well");
  test.assertEqual(JSON.stringify(msg), JSONifiedMessage,
      "JSONification of Message works as well");
};

/*
 *
 * var theURL = main.theURL; var testURL =
 * self.data.url('tests/change-more-bugs01.html');
 *
 * var contentScriptLibraries = { "bugzilla.redhat.com": [
 * self.data.url("util.js"), self.data.url("color.js"),
 * self.data.url("rhbzpage.js"), self.data.url("bzpage.js") ] };
 *
 * libbz.initialize(libbz.config, function () { pageMod.PageMod({ include: [
 * "https://bugzilla.redhat.com/show_bug.cgi?id=*" ], contentScriptWhen:
 * 'ready', contentScriptFile: contentScriptLibraries["bugzilla.redhat.com"],
 * onAttach: function onAttach(worker, msg) { console.log("worker: " + worker);
 * worker.on('message', function (msg) { messageHandler(worker, msg); }); } });
 * });
 *
 * pageMod.PageMod({ include: [ "https://bugzilla.redhat.com/process_bug.cgi" ],
 * contentScriptWhen: 'ready', contentScriptFile: self.data.url("skip-bug.js")
 * });
 */

var ensureSimplePageLoad = function(test) {
  console.log("testURL = " + testURL);
  testPageMod(test, testURL, [
    {
      include : [
        "*"
      ],
      contentScriptWhen : 'ready',
      contentScriptFile : [
          self.data.url("libPW.js"),
          self.data.url("simplePageWorker.js")
      ],
      onAttach : function onAttach(worker) {
        worker.on('message', function(msg) {
          switch (msg.cmd) {
          case "CallBack":
            worker
                .postMessage(new utilMod.Message("Main", null));
            break;
          default:
            console.error(msg);
          }
        });
      }
    }
  ], function(win, done) {
    test.assertNotEqual(win.document
        .getElementsByTagName("form")[0], null,
        "test of loading the page");
    done();
  });
};

var ensurePageLoadsWell = function(test) {
  var wm = Cc['@mozilla.org/appshell/window-mediator;1']
      .getService(Ci.nsIWindowMediator);
  var browserWindow = wm
      .getMostRecentWindow("navigator:browser");
  if (!browserWindow) {
    test
        .fail("page-mod tests: could not find the browser window, so "
            + "will not run. Use -a firefox to run the pagemod tests.");
    return null;
  }

  var loader = test.makeSandboxedLoader();
  var pageMod = loader.require("page-mod");
  var testDoc = {}, b = {}, tabBrowser = {}, newTab = {};

  pageMod.PageMod({
    include : [
      "*"
    ],
    contentScriptWhen : 'ready',
    contentScriptFile : [
        self.data.url("libPW.js"),
        self.data.url("pageWorker.js")
    ],
    onAttach : function onAttach(worker) {
      worker.on('message', function(msg) {
        switch (msg.cmd) {
        case "testReady":
          testDoc = b.contentWindow.wrappedJSObject.document;
          test.assertNotEqual(testDoc
              .getElementById("dupeid_action"), null,
              "test of DOM modifications of the page");
          pageMod.destroy();
          tabBrowser.removeTab(newTab);
          test.done();
          // the test itself
          break;
        default:
          main.messageHandler(worker, msg);
        }
      });
    }
  });

  tabBrowser = browserWindow.gBrowser;
  newTab = tabBrowser.addTab(testURL);
  tabBrowser.selectedTab = newTab;
  b = tabBrowser.getBrowserForTab(newTab);
};
