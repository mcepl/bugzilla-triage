/*global exports: false, require: false */
/*jslint plusplus: false */
"use strict";
var xrpc = require("xmlrpc");
var logMod = require("logger");
var selfMod = require("self");

var testGenerateTimeSheetDataLogs = {
  "2010-07-27+bugzilla.redhat.com+533567" : {
    "date" : "2010-07-27T21:28:47.103Z",
    "url" : "https://bugzilla.redhat.com/show_bug.cgi?id=533567",
    "title" : "KMS:RS480:X200M - GPU lockup (screen goes black)",
    "comment" : "removing filled in chip type for Guadec"
  },
  "2010-07-27+bugzilla.redhat.com+618769" : {
    "date" : "2010-07-27T21:30:18.845Z",
    "url" : "https://bugzilla.redhat.com/show_bug.cgi?id=618769",
    "title" : "gdm and display unstable with ATI FirePro V3700 graphics card",
    "comment" : "asking for logs"
  }
};
var testGenerateTimeSheetResultStr = "<!DOCTYPE html><html><head>\n<meta charset='utf-8'/>\n"
    + "<title>TimeSheet-2011-04-17</title>\n</head>\n<body>\n<h1>TimeSheet</h1>\n"
    + "<hr/><p><strong>2010-07-27</strong></p>\n<p><em>"
    + "<a href='https://bugzilla.redhat.com/show_bug.cgi?id=533567'>Bug RH/533567: "
    + "KMS:RS480:X200M - GPU lockup (screen goes black)</a> </em>\n<br/>removing filled "
    + "in chip type for Guadec</p>\n<p><em><a "
    + "href='https://bugzilla.redhat.com/show_bug.cgi?id=618769'>Bug RH/618769: "
    + "gdm and display unstable with ATI FirePro V3700 graphics card</a> </em>\n"
    + "<br/>asking for logs</p>\n</body></html>";

exports.ensureTimeSheetRecordsPrinter = function(test) {
  logMod.initialize(JSON.parse(selfMod.data
      .load("bugzillalabelAbbreviations.json")));
  test.assertEqual(logMod.timeSheetRecordsPrinter(
      testGenerateTimeSheetDataLogs, new Date("2011-04-17")),
      testGenerateTimeSheetResultStr,
      "Generates correct log report from given data.");
};
