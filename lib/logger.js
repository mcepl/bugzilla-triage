// Released under the MIT/X11 license
// http://www.opensource.org/licenses/mit-license.php
"use strict";
var fileMod = require("sdk/io/file");
var prompts = require("prompts");
var myStorage = require("sdk/simple-storage");

var EmptyLogsColor = "rgb(0, 255, 0)";
var FullLogsColor = "rgb(0, 40, 103)";

var abbsMap = {};

exports.initialize = function initialize() {
  if (!myStorage.storage.logs) {
    myStorage.storage.logs = {};
  }
};

exports.addLogRecord = function addLogRecord(rec) {
  if (myStorage.storage.logs[rec.key]
      && myStorage.storage.logs[rec.key].comment) {
    myStorage.storage.logs[rec.key].comment += "<br/>\n"
        + rec.comment;
  }
  else {
    myStorage.storage.logs[rec.key] = rec;
  }
};

exports.getAllRecords = function getAllRecords() {
    return myStorage.storage.logs;
};

exports.clearTimeSheet = function clearTimeSheet() {
  myStorage.storage.logs = {};
};

exports.importTimeSheet = function importTimeSheet() {
  var filename = prompts.promptFileOpenPicker();
  if (fileMod.exists(filename)) {
    var otherTS = JSON.parse(fileMod.read(filename));
    if (otherTS.logs) {
      for ( var rec in otherTS.logs) {
        myStorage.storage.logs[rec] = otherTS.logs[rec];
      }
    }
    else {
      console.error("This is not a log file!");
    }
  }
  else {
    console.error("File " + filename + " doesn't exist!");
  }
};

