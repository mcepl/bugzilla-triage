//Released under the MIT/X11 license
//http://www.opensource.org/licenses/mit-license.php

//Links to read through
//http://ehsanakhgari.org/blog/2010-01-07/bugzilla-tweaks-enhanced
//http://hg.mozilla.org/users/ehsan.akhgari_gmail.com/extensions/file/tip/bugzillatweaks
//http://hg.mozilla.org/users/ehsan.akhgari_gmail.com/extensions/file/ecfa0f028b81/bugzillatweaks/lib/main.js
//http://hg.mozilla.org/users/avarma_mozilla.com/atul-packages/file/42ac1e99a107/packages\
///facebook-acquaintances/lib/main.js#l11
//http://ehsanakhgari.org/blog/2010-05-31/my-experience-jetpack-sdk#comment-1253

"use strict";
var self = require("sdk/self");
var pageMod = require("sdk/page-mod");
var libbz = require("libbugzilla");
var tabs = require("sdk/tabs");
var logger = require("logger");
var Message = require("util").Message;
var contextMenu = require("sdk/context-menu");


/**
 *
 */
function skipThisPage(doc) {
  var stemURL = "https://HOSTNAME/show_bug.cgi?id=";
  var titleElems = doc.getElementsByTagName("title");
  var titleStr = titleElems[0].textContent;
  var REArr = new RegExp("[bB]ug\\s+([0-9]+)").exec(titleStr);
  var hostname = doc.location.hostname;
  if (REArr) {
    var bugNo = REArr[1];
    doc.location = stemURL.replace("HOSTNAME", hostname) + bugNo;
  }
}


var messageHandler = exports.messageHandler = function messageHandler(
    worker, msg) {
  switch (msg.cmd) {
  case "AddLogRecord":
    logger.addLogRecord(msg.data);
    break;
  case "ClearTS":
    logger.clearTimeSheet();
    break;
  case "ImportTS":
    logger.importTimeSheet();
    break;
  case "GetTSData":
    worker.postMessage(new Message("ReturningLogData",
      logger.getAllRecords()));
    break;
  case "GetInstalledPackages":
    // send message with packages back
    libbz.getInstalledPackages(msg.data, function(pkgsMsg) {
      worker.postMessage(pkgsMsg);
    });
    break;
  case "GetClipboard":
    libbz.getClipboard(function(clipboard) {
      worker.postMessage(new Message(msg.data, clipboard));
    });
    break;
  case "SetClipboard":
    libbz.setClipboard(msg.data);
    break;
  case "ChangeJSONURL":
    libbz.changeJSONURL();
    break;
  // Needed because Panel is add-on module
  case "OpenURLinPanel":
    libbz.openURLInNewPanel(msg.data);
    break;
  // Needed because of tabs.open()
  case "OpenURLinTab":
    libbz.openURLInNewTab(msg.data);
    break;
  // Needed because creating additional page-mods on remote bugzilla
  case "OpenBugUpstream":
    libbz.createUpstreamBug(msg.data.url, msg.data.subject,
        msg.data.comment);
    break;
  case "testReady":
    // we ignore it here, interesting only in unit test
    break;
  default:
    console.error(msg.toSource());
  }
};


var contentScriptLibraries = [
    self.data.url('tweaks/urltest.js'),
    self.data.url("lib/util.js"),
    self.data.url("lib/rpcutils.js"),
    self.data.url("lib/jumpNextBug.js"),
    self.data.url("lib/queries.js"),
    self.data.url("lib/collectingMetadata.js"),
    self.data.url("tweaks/preprocessDuplicates.js"),
    self.data.url("tweaks/viewSource.js"),
    self.data.url("lib/color.js"),
    self.data.url("tweaks/addNewLinks.js"),
    self.data.url("lib/bugzillaDOMFunctions.js"),
    self.data.url("rhlib/xorgBugCategories.js"),
    self.data.url("lib/otherButtons.js"),
    self.data.url("rhlib/makeBacktraceAttachment.js"),
    self.data.url("rhlib/fixingAttMIME.js"),
    self.data.url("lib/logging-front.js"),
    self.data.url('tweaks/bug-page-mod.js'),
    self.data.url('mozlib/mozpage.js'),
    self.data.url("rhlib/rhbzpage.js"),
    self.data.url("lib/bzpage.js")
];

libbz.initialize(function(config) {
  pageMod.PageMod({
    include : config.configData.bugPageMatch,
    contentScriptWhen : 'ready',
    contentScriptFile : contentScriptLibraries,
    onAttach : function onAttach(worker, msg) {
      worker.on('message', function(msg) {
        messageHandler(worker, msg);
      });
      worker.port.on('GetURL', function (command) {
        libbz.getURL(command.url,
          function(stuff) {
            worker.port.emit(command.backMessage,
                stuff);
          });
      });
    }
  });

  pageMod.PageMod({
    include : config.configData.skipMatch,
    contentScriptWhen : 'ready',
    contentScriptFile : [
        self.data.url("lib/util.js"),
        self.data.url("lib/skip-bug.js")
    ]
  });
});

//Allow toggling of CC event displays using a context menu entry
contextMenu.Item({
  label : "Toggle CC History",
  contentScriptFile : [
      self.data.url('tweaks/urltest.js'),
      self.data.url('tweaks/cc-context.js')
  ]
});


contextMenu.Item({
  label : "Copy Check-in Comment",
  contentScriptFile : [
      self.data.url('tweaks/urltest.js'),
      self.data.url('tweaks/checkin-context.js')
  ],
  onMessage : function(comment) {
    require("sdk/clipboard").set(comment);
  }
});
//tabs.open("https://bugzilla.redhat.com/show_bug.cgi?id=1033263");
