/* global exports: false, require: false, console: false, Cc: false, Ci: false */
/* jslint onevar: false */
// Released under the MIT/X11 license
// http://www.opensource.org/licenses/mit-license.php
"use strict";
// ==============================================================
var Cc = require("chrome").Cc;
var Ci = require("chrome").Ci;
var urlMod = require("sdk/url");

/**
 * get parameters of URL as an object (name, value)
 */
function getParamsFromURL(url, base) {
  if (!url || (url.toString().length === 0)) {
    throw new Error("Missing URL value!");
  }

  if (!(url instanceof urlMod.URL)) {
    url = new urlMod.URL(url.toString(), base);
  }

  var paramsArr = url.path.split("?");
  if (paramsArr.length === 1) {
    return {};
  }

  // get convert URL parameters to an Object
  var params = {}, s = [];
  paramsArr[1].split('&').forEach(function(par) {
    s = par.split('=');
    params[s[0]] = s[1];
  });
  return params;
}

/**
 * Get a bug no from URL ... fails with aliases It should theoretically belong
 * to bzpage.js, but we don't have unit tests there yet, so keeping here.
 *
 * @param url
 *          String with URL to be analyzed
 * @return String with the bug ID (hopefully number, but not for aliases)
 */
exports.getBugNo = function getBugNo(url) {
  var params = getParamsFromURL(url);
  if (params && params.id) {
    return params.id;
  }
};

/**
 * format date to be in ISO format (just day part)
 *
 * @param date
 * @return string with the formatted date
 */
exports.getISODate = function getISODate(dateStr) {
  function pad(n) {
    return n < 10 ? '0' + n : n;
  }
  var date = new Date(dateStr);
  return date.getFullYear() + '-' + pad(date.getMonth() + 1)
      + '-' + pad(date.getDate());
};

/**
 * object to pack messaging. Use as in postMessage(new Message("GetPassword", {
 * login: login, hostname: location.hostname }));
 */
exports.Message = function Message(cmd, data) {
  this.cmd = cmd;
  this.data = data;
};
