VERNO=$(shell rhino -e 'print(JSON.parse(readFile("package.json")).version);')
WEBDIR=/home/matej/Dokumenty/website/fedorahosted/
UPSTREAM_XPI_URL=https://fedorahosted.org/released/bugzilla-triage-scripts/bugzilla-triage-$(VERNO).xpi
UPDATE_URL=https://fedorahosted.org/released/bugzilla-triage-scripts/update.rdf

all: bugzilla-triage-$(VERNO).xpi update.rdf

amo: bugzilla-triage.xpi

install: local-install
	make -C $(WEBDIR)
	websync

local-install: bugzilla-triage-$(VERNO).xpi update.rdf
	rsync -av bugzilla-triage-*.xpi $(WEBDIR)
	rsync -av jsons/Config_data.json $(WEBDIR)

bugzilla-triage-$(VERNO).xpi: package.json lib/*.js
	jpm xpi
	unzip -q bugzilla-triage.xpi install.rdf
	sed -i -e '/em:minVersion/s!>[0-9.]*<!>7.0<!' install.rdf
	zip -9qmT bugzilla-triage.xpi install.rdf 
	tidy -i -xml -utf8 -m bugzilla-triage.update.rdf
	mv bugzilla-triage.xpi bugzilla-triage-$(VERNO).xpi

bugzilla-triage.xpi: package.json lib/*.js
	jpm xpi --strip-sdk

update.rdf: bugzilla-triage-$(VERNO).xpi
	tidy -i -xml -utf8 -m update.rdf

clean:
	find . -name \*~ -print -delete
	rm -f -v *.xpi bugzilla-triage.update.rdf
