Firefox addon supporting bug triage on bugzilla
====================================

Set of buttons and other functionality added to bugzilla
show_bug.cgi pages to make bug triage easier.

For more description see the [author’s
blog](http://mcepl.blogspot.com/).
