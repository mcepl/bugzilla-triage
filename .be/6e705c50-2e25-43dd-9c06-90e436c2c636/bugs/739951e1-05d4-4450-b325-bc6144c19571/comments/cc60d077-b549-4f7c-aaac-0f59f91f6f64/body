Hi all,

If you don't know what RPC is, you can ignore this e-mail.

Below are the RPC changes for the upcoming release of Bugzilla 4.4. A
development site is available at https://partner-bugzilla.redhat.com/ to
ensure that your scripts will continue to work as expected.

The upgrade will take place on the evening of April 27th (EDT).

The biggest change is that we are removing most RedHat.* bugzilla.* and
Flag.get RPC calls. A complete list of the calls being removed is at the
bottom of this e-mail. Accessing one of the removed calls will generate
a HTTP 410 error message, and mention the new RPC call that you should use.

With a few exceptions (bugzilla.getCompInfo being one), you should be
able to migrate any scripts to use the new calls any time before the Red
Hat Bugzilla upgrade to ensure a uninterrupted operation.

Documentation of RPC calls can be found at
https://partner-bugzilla.redhat.com/docs/en/html/api/

RPC changes from upstream 4.4
-----------------------------
 - Bug.* returns arrays for components, versions and aliases
 - Bug.* returns target_release array
 - Bug.search supports searching on keywords, dependencies, blocks
 - Bug.search supports quick searches, saved searches and advanced searches
 - Group.get has been added
 - Component.* and Flag.* have been added

RPC changes from Red Hat Bugzilla 4.2
-------------------------------------
 - This list excludes upstream changes from 4.2 that we inherit from
upstream

 - User.get and User.update is now the upstream version. The Red Hat
(4.2) version is no longer available. User.get returns less information
about groups the user has (id, name, is_direct and description only).
User.update now changes groups differently. See the documentation for
details.

 - Bug.update doesn't update external trackers any more. Use the
ExternalBugs module instead.

 - Product.get no longer accepts names-only as an option. Use sub fields
instead.

 - Component.get will now only accept ids and names values as an array
(of integers and hashes respectively). It will also silently ignore
components and products that don't exist, or you don't have access too.

 - Component.update and Component.create will now work on closed products.

 - Documentation for the component calls is now at
https://bugzilla.redhat.com/docs/en/html/api/extensions/RedHat/lib/WebService/Component.html
. It is very likely that Bugzilla 5.0 will have upstream supported
calls. For this reason, we have marked the RPC methods as experimental.

If you notice any bugs, check to see if it has already been reported at
https://url.corp.redhat.com/bz44check .

If it isn't, you can file a new report at
https://url.corp.redhat.com/bz44report


The following RPC calls will be removed in favour of the upstream
supported versions:

OLD RPC CALL                        REPLACED WITH
bugzilla.addAttachment              Bug.add_attachment
bugzilla.addComment                 Bug.add_comment
bugzilla.addComponent               Component.create [1]
bugzilla.addIT                      no replacement
bugzilla.addUser                    User.create
bugzilla.changeAssignment           Bug.update
bugzilla.changeStatus               Bug.update
bugzilla.closeBug                   Bug.update
bugzilla.createBug                  Bug.create
bugzilla.deleteIT                   no replacement
bugzilla.disableAccount             User.update
bugzilla.editComponent              Component.update [1]
bugzilla.getAllGroups               RedHat.getAllGroups [1]
bugzilla.getBugActivity             Bug.history
bugzilla.getBug                     Bug.get
bugzilla.getBugCVS                  Bug.get
bugzilla.getBugFields               Bug.fields
bugzilla.getBugModified             Bug.search
bugzilla.getBugSimple               Bug.get
bugzilla.getCompInfo                Component.get [1]
bugzilla.getProdCompDetails         Product.get
bugzilla.getProdInfo                Product.get
bugzilla.getProductDetails          Product.get
bugzilla.getReleaseComponents       Releases.getReleaseComponents [1]
bugzilla.getRelease                 Releases.getRelease [1]
bugzilla.getReleaseSchedule         Releases.getReleaseSchedule [1]
bugzilla.idToName                   User.get
bugzilla.issueExists                no replacement
bugzilla.login                      no replacement
bugzilla.nameToId                   User.get
bugzilla.runQuery                   Bug.search
bugzilla.updateAttachMimeType       RedHat.updateAttachMimeType [1]
bugzilla.updateCC                   Bug.update
bugzilla.updateDepends              Bug.update
bugzilla.updateFlagRequestees       Flag.update
bugzilla.updateFlags                Flag.update
bugzilla.updateKeywords             Bug.update
bugzilla.updateMilestone            Bug.update
bugzilla.updatePerms                User.update
bugzilla.updatePriority             Bug.update
bugzilla.updateReleaseComponents    Releases.updateReleaseComponents [1]
bugzilla.updateRelease              Releases.updateRelease [1]
bugzilla.updateReleaseSchedule      Releases.updateReleaseSchedule [1]
bugzilla.updateSeverity             Bug.update
bugzilla.userInfo                   User.get
Flag.get                            Bug.get
RedHat.addAttachment                Bug.add_attachment
RedHat.addComment                   Bug.add_comment
RedHat.addComponent                 Component.create [1]
RedHat.addUser                      User.create
RedHat.changeAssignment             Bug.update
RedHat.changeStatus                 Bug.update
RedHat.closeBug                     Bug.update
RedHat.createBug                    Bug.create
RedHat.disableAccount               User.update
RedHat.editComponent                Component.update [1]
RedHat.getBugActivity               Bug.history
RedHat.getBug                       Bug.get
RedHat.getBugCVS                    Bug.get
RedHat.getBugFields                 Bug.fields
RedHat.getBugModified               Bug.search
RedHat.getBugSimple                 Bug.get
RedHat.getCompInfo                  Component.get [1]
RedHat.getProdCompDetails           Product.get
RedHat.getProdInfo                  Product.get
RedHat.getProductDetails            Product.get
RedHat.idToName                     User.get
RedHat.login                        no replacement
RedHat.nameToId                     User.get
RedHat.runQuery                     Bug.search
RedHat.updateCC                     Bug.update
RedHat.updateDepends                Bug.update
RedHat.updateFlagRequestees         Flag.update
RedHat.updateFlags                  Flag.update
RedHat.updateKeywords               Bug.update
RedHat.updateMilestone              Bug.update
RedHat.updatePerms                  User.update
RedHat.updatePriority               Bug.update
RedHat.updateSeverity               Bug.update
RedHat.userInfo                     User.get

[1] These are not upstream written or supported, but remove the
duplication that we have
[2] taken from the upstream release notes directly.

Kind Regards,
-- 
Róman Joost
Product Manager, HSS - Infrastructure Engineering & Development (Brisbane)
irc: rjoost #bugzilla | email: rjoost@redhat.com | tz: UTC+10
